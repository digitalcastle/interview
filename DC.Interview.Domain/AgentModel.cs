namespace DC.Interview.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IAgentDbContext : IDisposable
    {
        // DB Context methods
        DbChangeTracker ChangeTracker { get; }
        DbContextConfiguration Configuration { get; }
        Database Database { get; }
        DbEntityEntry Entry(object entity);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        IEnumerable<DbEntityValidationResult> GetValidationErrors();
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        // DB Sets
        DbSet<Agent> Agents { get; set; }
        DbSet<Agency> Agencies { get; set; }
    }



    public class AgentDbContext : DbContext, IAgentDbContext
    {
        // Your context has been configured to use a 'AgentModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'DC.Interview.Domain.AgentModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'AgentModel' 
        // connection string in the application configuration file.
        public AgentDbContext()
            : base("name=AgentModel")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Agent> Agents { get; set; }
        public virtual DbSet<Agency> Agencies { get; set; }
    }

    public class Agent
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? AgencyId { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset Updated { get; set; }
        public virtual Agency Agency { get; set; }

    }

    public class Agency
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }            
    }

}