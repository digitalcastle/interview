﻿using log4net;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.NamedScope;
using DC.Interview.Domain;

namespace DC.Interview.Service.Ioc
{
    public class ServicesBindingModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILog>().ToMethod(ctx => LogManager.GetLogger(ctx.Request.Target.Member.DeclaringType));
            //Bind<IAgentService>().To<AgentService>().InCallScope();
            Bind<IAgentDbContext>().To<AgentDbContext>().InCallScope();
        }
    }
}
